package com.ernturn.imgurtest.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import com.ernturn.imgurtest.R;

/**
 * Created by ernest on 9/14/2015.
 */
public class CustomProgressDialog extends Dialog {
    String messgage;
    TextView textView;

    public CustomProgressDialog(Context context, String message) {
        super(context);
        this.messgage = message;
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void show() {
        super.show();
        setContentView(R.layout.dialog_simple_progress);
        this.setCancelable(false);
        textView = (TextView)findViewById(R.id.dialog_message);
        textView.setText(this.messgage);
    }
}
