package com.ernturn.imgurtest.utils;

import com.ernturn.imgurtest.model.ImgurBasic;
import com.ernturn.imgurtest.model.ImgurImage;
import com.ernturn.imgurtest.model.ImgurImages;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by ernes on 5/17/2017.
 */

public interface ImgurApiInterface {

    @GET("account/me/images")
    Call<ImgurImages> getImages(@Header("Authorization") String authorization);

    @Multipart
    @POST("image")
    Call<ImgurImage> uploadImage(@Header("Authorization") String authorization, @Part("description") String description, @Part("image") RequestBody image);

    @DELETE("image/{imageHash}")
    Call<ImgurBasic> deleteImage(@Header("Authorization") String authorization, @Path("imageHash") String imageHash);
}
