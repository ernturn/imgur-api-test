package com.ernturn.imgurtest.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

/**
 * Created by ernes on 5/15/2017.
 */

public class Common {

    public final static String FULL_RES_URL = "FULL_RES_URL";
    public final static String DELETE_HASH = "DELETE_HASH";
    public static final int PICK_FROM_GALLARY = 1000;
    public static final int IMAGE_DELETE = 1001;
    public final static int IMAGE_DELETED = 2000;

    public static void showToast(Context context, String Mesg){
        try{
            Toast.makeText(context, Mesg, Toast.LENGTH_SHORT).show();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static final boolean isInternetAvailable(Context ctx) {
        try{
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
