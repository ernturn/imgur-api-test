package com.ernturn.imgurtest.utils;

/**
 * Created by ernes on 5/15/2017.
 */

public class ImgurUtils {

    /**
     * API Credentials used for logging in.
     */
    public final static String IMGUR_CLIENT_ID = "d80d1fcfe563d33";
    public final static String REDIRECT_URL = "https://imgur.com";
    public final static String ACCESS_DENIED = "error=access_denied";

    /**
     * Endpoints for authorization, getting images, uploading images, and delete images
     */
    public static final String IMGUR_ENDPOINT ="https://api.imgur.com/3/";
    public static final String AUTHORIZATION_ENDPOINT ="https://api.imgur.com/oauth2/authorize";

    /**
     * To hold the users credentials:
     * accessToken  to use for authenticating with the API.
     */
    public static String accessToken;


    /**
     * To get an authorization token.
     */
    public static String getAuthorizationUrl(){
        return AUTHORIZATION_ENDPOINT
                +"?client_id=" + IMGUR_CLIENT_ID
                + "&response_type=token&redirect_uri="
                + REDIRECT_URL;
    }
}
