package com.ernturn.imgurtest.application;

import android.app.Application;

import com.ernturn.imgurtest.utils.ImgurApiInterface;

/**
 * Created by ernes on 5/17/2017.
 */

public class ImgurTestApplication extends Application {
    private ImgurApiInterface apiService;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public ImgurApiInterface getApiService() {
        return apiService;
    }

    public void setApiService(ImgurApiInterface apiService) {
        this.apiService = apiService;
    }
}
