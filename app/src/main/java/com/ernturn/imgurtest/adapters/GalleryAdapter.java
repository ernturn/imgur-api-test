package com.ernturn.imgurtest.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ernturn.imgurtest.R;
import com.ernturn.imgurtest.model.ImgurImage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ernes on 5/15/2017.
 */

public class GalleryAdapter extends BaseAdapter{
    private final Activity context;
    private static LayoutInflater inflater = null;
    private ArrayList<ImgurImage> images = new ArrayList<>();

    public GalleryAdapter(Activity context, ArrayList<ImgurImage> images) {
        // TODO Auto-generated constructor stub

        this.context = context;
        this.images = images;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public GalleryAdapter(Activity context) {
        // TODO Auto-generated constructor stub

        this.context = context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public class Holder
    {
        ImageView mainImage;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        ImgurImage img = images.get(position);

        rowView = inflater.inflate(R.layout.item_gallery, null);
        holder.mainImage = (ImageView) rowView.findViewById(R.id.mainImage);

        //Using Picasso library to load in thumbnails to be displayed in the grid
        Picasso.with(context)
                .load(img.getThumbLink())
                .placeholder(R.drawable.ic_image)
                .resize(96, 96)
                .centerCrop()
                .error(R.drawable.ic_broken_image)
                .into(holder.mainImage);

        return rowView;
    }

    public void addImage(ImgurImage image) {
        images.add(image);
    }

    public void clear(){
        images.clear();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return images.size();
    }

    @Override
    public ImgurImage getItem(int position) {
        // TODO Auto-generated method stub
        return images.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return images.get(position).getId().hashCode();
    }

    private class AdapterImage {
        public ImgurImage image;
        public AdapterImage(ImgurImage i) {
            this.image = i;
        }
    }
}
