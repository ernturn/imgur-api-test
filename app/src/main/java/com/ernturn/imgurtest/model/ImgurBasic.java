package com.ernturn.imgurtest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ernes on 5/17/2017.
 */

public class ImgurBasic {
    @SerializedName("data")
    @Expose
    private Boolean data;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public Boolean getData() {
        return data;
    }

    public Integer getStatus() {
        return status;
    }

    public Boolean getSuccess() {
        return success;
    }
}
