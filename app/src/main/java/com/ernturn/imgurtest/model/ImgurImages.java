package com.ernturn.imgurtest.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by ernes on 5/17/2017.
 */

public class ImgurImages {
    @Expose
    private ArrayList<ImgurImage> data = new ArrayList<>();

    /**
     *
     * @return
     * The data
     */
    public ArrayList<ImgurImage> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(ArrayList<ImgurImage> data) {
        this.data = data;
    }
}
