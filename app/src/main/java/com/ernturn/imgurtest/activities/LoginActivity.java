package com.ernturn.imgurtest.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.ernturn.imgurtest.R;
import com.ernturn.imgurtest.utils.Common;
import com.ernturn.imgurtest.utils.ImgurUtils;

public class LoginActivity extends BaseActivity {

    private WebView mWebView;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mWebView = (WebView) findViewById(R.id.imgur_webview);
        mProgressBar = (ProgressBar) findViewById(R.id.progressView);

        showProgress();

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);

        String authorizationUrl = ImgurUtils.getAuthorizationUrl();
        mWebView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideProgress();
            }

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                if (!Common.isInternetAvailable(LoginActivity.this)) {
                    finish();
                    return false;
                } else {
                    if (url != null && url.startsWith(ImgurUtils.REDIRECT_URL)) { //Once access is granted get the access token
                        getAccessToken(url);
                    }
                    else if(url.contains(ImgurUtils.ACCESS_DENIED)){
                        Common.showToast(LoginActivity.this, "Access Denied!");
                        LoginActivity.this.finish();
                    }else {
                        webView.loadUrl(url);
                    }
                }
                return true;
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError (WebView view, int errorCode,
                                         String description, String failingUrl) {
                Common.showToast(LoginActivity.this, "Please check your internet connection");
                finish();
            }
        });

        //Load the authorization URL
        if(ImgurUtils.accessToken != null){
            mWebView.clearCache(true);
            mWebView.clearHistory();
            clearCookies(LoginActivity.this);
        }
        mWebView.loadUrl(authorizationUrl);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * This method parses the FULL_RES_URL with the access token so it can be read and saved.
     *
     * @param url FULL_RES_URL containing the access token
     */
    private void getAccessToken(String url){
        String newUrl = url.replace("#", "?");
        Uri uri = Uri.parse(newUrl);
        ImgurUtils.accessToken = uri.getQueryParameter("access_token");
        goToGalleryActivity();
    }

    /**
     * Goes on to new activity using Intent
     */
    private void goToGalleryActivity(){
        Intent myIntent = new Intent(LoginActivity.this, GalleryActivity.class);
        startActivity(myIntent);
        finish();
    }


    /**
     * Simple methods to show and hide the top progress bar
     */
    private void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }


    /**
     * Function needed to log user out of webview so a new user can log in.
     * @param context
     */
    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context)
    {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        }
        else{
            CookieSyncManager cookieSyncMngr=CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }
}
