package com.ernturn.imgurtest.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.ernturn.imgurtest.R;
import com.ernturn.imgurtest.adapters.GalleryAdapter;
import com.ernturn.imgurtest.application.ImgurTestApplication;
import com.ernturn.imgurtest.model.ImgurImage;
import com.ernturn.imgurtest.model.ImgurImages;
import com.ernturn.imgurtest.utils.Common;
import com.ernturn.imgurtest.utils.CustomProgressDialog;
import com.ernturn.imgurtest.utils.ImgurApiInterface;
import com.ernturn.imgurtest.utils.ImgurUtils;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ernturn.imgurtest.utils.Common.IMAGE_DELETE;
import static com.ernturn.imgurtest.utils.Common.PICK_FROM_GALLARY;

/**
 * Created by ernes on 5/15/2017.
 */

public class GalleryActivity extends BaseActivity {

    private GalleryAdapter adapter;
    private GridView grid;
    private ProgressBar progressView;
    private ImgurApiInterface apiService;
    ArrayList<ImgurImage> images = new ArrayList<>();

    //Permission variable
    private final int PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 0x00;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        progressView = (ProgressBar) findViewById(R.id.progressView);
        grid = (GridView) findViewById(R.id.gridView);
        createService();
        adapter = new GalleryAdapter(GalleryActivity.this, new ArrayList<ImgurImage>());
        grid.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
        grid.setAdapter(adapter);
        getImages();
    }

    @Override
    public void onResume() {
        super.onResume();

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() { //if any item is clicked in the gridview show a full resolution image in new activity
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImgurImage selectedImage = adapter.getItem(position);
                Intent myIntent = new Intent(GalleryActivity.this, FullImageActivity.class);
                myIntent.putExtra(Common.FULL_RES_URL, selectedImage.getLink());
                myIntent.putExtra(Common.DELETE_HASH, selectedImage.getDeletehash());
                startActivityForResult(myIntent, IMAGE_DELETE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICK_FROM_GALLARY: //upload image to the API using multipart

                if (resultCode == Activity.RESULT_OK){
                    //pick image from gallery
                    Uri selectedImage = data.getData();
                    String path = getRealPathFromURI(selectedImage);
                    if(path != null){
                        MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");
                        File file = new File(path);
                        RequestBody requestBody = RequestBody.create(MEDIA_TYPE_JPG, file);
                        final CustomProgressDialog progressDialog = new CustomProgressDialog(GalleryActivity.this, getString(R.string.uploading_image));
                        progressDialog.show();
                        Call<ImgurImage> call = apiService.uploadImage("Bearer " + ImgurUtils.accessToken, "test", requestBody);
                        call.enqueue(new Callback<ImgurImage>() {
                            @Override
                            public void onResponse(Call<ImgurImage>call, Response<ImgurImage> response) {
                                progressDialog.dismiss();
                                int statusCode = response.code();
                                ImgurImage image = response.body();
                                refreshImages();
                            }

                            @Override
                            public void onFailure(Call<ImgurImage>call, Throwable t) {
                                // Log error here since request failed
                            }
                        });
                    }
                    else{
                        Common.showToast(GalleryActivity.this, GalleryActivity.this.getString(R.string.common_error_message));
                    }
                }
                break;

            case Common.IMAGE_DELETE:
                if (resultCode == Common.IMAGE_DELETED){
                    refreshImages();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                //check phone OS
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    checkStoragePermission();
                else
                    startPhotoPickerIntent();
                break;
            case R.id.menu_refresh:
                refreshImages();
                break;
            case R.id.menu_log_out:
                Intent i = new Intent(GalleryActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    /**
     * Creates the Retofit API service and stores it as an application-wide value.
     */
    public void createService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ImgurUtils.IMGUR_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService = retrofit.create(ImgurApiInterface.class);

        ((ImgurTestApplication) getApplication()).setApiService(apiService);
    }

    /**
     * Checking and asking app for storage permission API >= 23
     */
    @TargetApi(23)
    public void checkStoragePermission() {
        int hasWriteContactsPermission = GalleryActivity.this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {//if permission is not granted, show dialog
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {//show why permission is needed
                Snackbar snackbar = Snackbar
                        .make(findViewById(android.R.id.content), getString(R.string.permission_storage_rationale), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.btn_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                                        PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
                            }
                        });
                snackbar.show();
                return;
            }
            else{
                requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
            }

            return;
        }
        else{//if permission is already granted, go to photo picker
            startPhotoPickerIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startPhotoPickerIntent();

                }
            }
        }
    }

    /**
     * Gets images from the API and set them into an adapter to be shown in gridview
     */
    public void getImages(){
        Call<ImgurImages> call = apiService.getImages("Bearer " + ImgurUtils.accessToken);
        call.enqueue(new Callback<ImgurImages>() {
            @Override
            public void onResponse(Call<ImgurImages>call, Response<ImgurImages> response) {
                int statusCode = response.code();
                ImgurImages images = response.body();
                for(int x = 0; x < images.getData().size(); x++){
                    ImgurImage image = images.getData().get(x);
                    adapter.addImage(image);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ImgurImages>call, Throwable t) {
                // Log error here since request failed
            }
        });
    }

    /**
     * Simple refresh function in case an image is added or deleted
     */
    public void refreshImages(){
        adapter.clear();
        adapter.notifyDataSetChanged();
        getImages();
    }

    /**
     * Start intent to select image to upload
     */
    private void startPhotoPickerIntent(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        Intent chooser = Intent.createChooser(intent, "Choose a Picture");
        startActivityForResult(chooser, Common.PICK_FROM_GALLARY);
    }

    /**
     *
     * @param imageUri - uri of the selected image to upload
     * @return path of the image given
     */
    private String getRealPathFromURI(Uri imageUri) {
        try{
            Cursor cursor = getContentResolver().query(imageUri, null, null, null, null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":")+1);
            cursor.close();

            cursor = getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            return path;
        }
        catch(NullPointerException e){
            e.printStackTrace();
            return null;
        }

    }
}
