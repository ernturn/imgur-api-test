package com.ernturn.imgurtest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.ernturn.imgurtest.R;
import com.ernturn.imgurtest.application.ImgurTestApplication;
import com.ernturn.imgurtest.model.ImgurBasic;
import com.ernturn.imgurtest.utils.Common;
import com.ernturn.imgurtest.utils.CustomProgressDialog;
import com.ernturn.imgurtest.utils.ImgurApiInterface;
import com.ernturn.imgurtest.utils.ImgurUtils;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ernes on 5/16/2017.
 */

public class FullImageActivity extends BaseActivity {
    private ImageView mImageView;
    private String url;
    private String deleteHash;
    private ImgurApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
        mImageView = (ImageView) findViewById(R.id.image_view);

        Intent intent = getIntent();
        url = intent.getStringExtra(Common.FULL_RES_URL);
        deleteHash = intent.getStringExtra(Common.DELETE_HASH);
        apiService = ((ImgurTestApplication) getApplication()).getApiService();


        Picasso.with(this)
                .load(url)
                .placeholder(R.drawable.ic_image)
                .error(R.drawable.ic_broken_image)
                .into(mImageView);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_fullimage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                deleteImage();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    /**
     * Delete current image that is showing, then go back to gallery activity to refresh the images
     */
    private void deleteImage(){
        final CustomProgressDialog progressDialog = new CustomProgressDialog(FullImageActivity.this, getString(R.string.deleting_image));
        progressDialog.show();
        Call<ImgurBasic> call = apiService.deleteImage("Bearer " + ImgurUtils.accessToken, deleteHash);
        call.enqueue(new Callback<ImgurBasic>() {
            @Override
            public void onResponse(Call<ImgurBasic>call, Response<ImgurBasic> response) {
                progressDialog.dismiss();
                int statusCode = response.code();
                ImgurBasic basic = response.body();
                Intent resultIntent = new Intent();
                setResult(Common.IMAGE_DELETED, resultIntent);
                finish();
            }

            @Override
            public void onFailure(Call<ImgurBasic>call, Throwable t) {
                // Log error here since request failed
            }
        });
    }
}
